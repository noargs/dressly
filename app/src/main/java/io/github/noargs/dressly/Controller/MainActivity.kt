package io.github.noargs.dressly.Controller

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import io.github.noargs.dressly.Adapters.CategoryAdapter
import io.github.noargs.dressly.Adapters.CategoryRecyleAdapter
import io.github.noargs.dressly.R
import io.github.noargs.dressly.Model.Category
import io.github.noargs.dressly.Services.DataService
import io.github.noargs.dressly.Utilities.EXTRA_CATEGORY
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

//    lateinit var adapter : CategoryAdapter

    lateinit var adapter : CategoryRecyleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // using ListView
//        adapter = CategoryAdapter(this, DataService.categories)
//
//        categoryListView.adapter = adapter
        
//        categoryListView.setOnItemClickListener{ adapterView, view, i, l ->
//            val category = DataService.categories[i]
//            Toast.makeText(this, "you clicked on the ${category.title} cell", Toast.LENGTH_SHORT).show()
//        }

        // using RecyleView [RecyleView often used now instead of ListVew]
        // adapter = CategoryRecyleAdapter(this, DataService.categories)

        // with click listener
        adapter = CategoryRecyleAdapter(this, DataService.categories){category ->
//            println(category.title)
            val productIntent = Intent(this, ProductActivity::class.java)
            productIntent.putExtra(EXTRA_CATEGORY, category.title)
            startActivity(productIntent)
        }
        categoryListView.adapter = adapter

        val layoutManager = LinearLayoutManager(this)
        categoryListView.layoutManager = layoutManager
        categoryListView.setHasFixedSize(true)


    }
}
