package io.github.noargs.dressly.Controller

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import io.github.noargs.dressly.Adapters.ProductAdapter
import io.github.noargs.dressly.R
import io.github.noargs.dressly.Services.DataService
import io.github.noargs.dressly.Utilities.EXTRA_CATEGORY
import kotlinx.android.synthetic.main.activity_product.*


class ProductActivity : AppCompatActivity() {

    lateinit var adapter : ProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        val categoryType = intent.getStringExtra(EXTRA_CATEGORY)
        println(categoryType)

        adapter = ProductAdapter(this, DataService.getProducts(categoryType))

        var spanCount = 2
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            spanCount = 3
        }

        val screenSize = resources.configuration.screenHeightDp
        if(screenSize > 720){
            spanCount = 3
        }


        val layoutManager = GridLayoutManager(this, spanCount)
        productLisView.layoutManager = layoutManager
        productLisView.adapter = adapter
    }
}
