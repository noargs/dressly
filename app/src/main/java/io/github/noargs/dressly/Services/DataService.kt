package io.github.noargs.dressly.Services

import io.github.noargs.dressly.Model.Category
import io.github.noargs.dressly.Model.Product

object DataService {

    val categories = listOf(
        Category("SHIRTS", "shirtimage"),
        Category("HOODIES", "hoodieimage"),
        Category("HATS", "hatimage"),
        Category("DIGITAL", "digitalgoodsimage")
    )

    val hats = listOf(
        Product("Dressly Graphic Beanie", "$18", "hat1"),
        Product("Dressly Hat Black", "$20", "hat2"),
        Product("Dressly Hat White", "$18", "hat3"),
        Product("Dressly Hat Snapback", "$22", "hat4"),
        Product("Dressly Graphic Beanie", "$18", "hat1"),
        Product("Dressly Hat Black", "$20", "hat2"),
        Product("Dressly Hat White", "$18", "hat3"),
        Product("Dressly Hat Snapback", "$22", "hat4")
    )

    val hoodies = listOf(
        Product("Dressly Hoodie Gray", "$28", "hoodie1"),
        Product("Dressly Hoodie Red", "$32", "hoodie2"),
        Product("Dressly Hoodie Silver", "$15", "hoodie3"),
        Product("Dressly Hoodie Black", "$12", "hoodie4"),
        Product("Dressly Hoodie Gray", "$28", "hoodie1"),
        Product("Dressly Hoodie Red", "$32", "hoodie2"),
        Product("Dressly Hoodie Silver", "$15", "hoodie3"),
        Product("Dressly Hoodie Black", "$12", "hoodie4")
    )

    val shirts = listOf(
        Product("Dressly Shirt Black", "$28", "shirt1"),
        Product("Dressly Badge Light Gray", "$32", "shirt2"),
        Product("Dressly Logo Shirt Red", "$15", "shirt3"),
        Product("Dressly Hustle", "$12", "shirt4"),
        Product("Dressly Kickflip Studios", "$18", "shirt5"),
        Product("Dressly Shirt Black", "$28", "shirt1"),
        Product("Dressly Badge Light Gray", "$32", "shirt2"),
        Product("Dressly Logo Shirt Red", "$15", "shirt3"),
        Product("Dressly Hustle", "$12", "shirt4"),
        Product("Dressly Kickflip Studios", "$18", "shirt5")
    )

    val digitalGood = listOf<Product>()

    fun getProducts(category: String) : List<Product>{
        return when(category){
            "SHIRTS" ->  shirts
            "HATS" ->  hats
            "HOODIES" -> hoodies
            else ->  digitalGood
        }
    }
}